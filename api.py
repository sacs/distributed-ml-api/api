class LocalMain():
    """
    Defines the data distribution according to the node's rank and the world size
    """
    def __init__(self, rank, world_size):
        super().__init__()
        self.rank = int(rank)
        self.world_size = int(world_size)

    def step(self):
        """ Performs a step of training on the local data. logging/testing can also be done here
            rtype: void
        """
        raise NotImplementedError

    def get_net(self):
        """
            returns: the neural net being used by the class
            rtype: torch.nn.Module
        """
        raise NotImplementedError

    def get_epoch(self):
        """
            returns: the current training epoch
            rtype: int
        """
        raise NotImplementedError


class Network():
    """
        This class defines the network properties:
            1. the communication protocol
            2. the network topology
            3. the encryption/decryption
    """

    def neighbors(self, rank):
        """
            Gives the neighbors of a node

            :param rank: node's rank on its machine

            returns: the neighbors
            rtype: collection(int)
        """
        raise NotImplementedError

    def addr(self, rank):
        """
            Gives the binding address for a node's router

            :param rank: node's rank on its machine

            returns: the binding address
            rtype: str
        """
        return f'tcp://127.0.0.1:{20000 + rank}'

    def encrypt(self, s: str):
        """
            Encrypts the given string
            Defaults to utf-8 encoding

            returns: the encrypted string
            rtype: bytes
        """
        return s.encode('utf8')

    def decrypt(self, b: bytes):
        """
            Decrypts the given bytes
            Defaults to utf-8 decoding

            returns: the decrypted string
            rtype: str
        """
        return b.decode('utf8')
