import json, zmq
from collections import deque
import util, node

class DPSGDNode(node.Node):
    def __init__(self, rank, nodes, ML, nt, iterations, lr, logging_interval, *args):
        super().__init__(rank, nodes, ML, nt, iterations, lr, logging_interval, *args)
        
        self.peer_deques = dict()
        self.peer_sockets = {}

        neighbors = nt.neighbors(self.rank)

        for rank in neighbors:
            id = str(rank).encode()
            self.barrier[id] = False
            req = self.context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.connect(nt.addr(rank))
            self.peer_sockets[id] = req
            self.peer_deques[id] = deque()

        for socket in self.peer_sockets.values():
            socket.send(b'HELLO')

        self.recv_loop()

    def send_model(self):
        data = util.serialize_model(self.ml.get_net())
        data['degree'] = len(self.peer_deques)

        to_send = self.nt.encrypt(json.dumps(data))
        self.log.add_sent(len(to_send) * len(self.peer_sockets))

        for sock in self.peer_sockets.values():
            sock.send(to_send)

    def recv_loop(self):
        while True:
            sender, recv = self.router.recv_multipart()
            self.log.add_recvd(len(recv))
        
            if recv ==b'HELLO':
                self.barrier[sender] = True
                if self.nb_connected() == len(self.peer_sockets):
                    util.Looper(self, self.logging_interval).start()
                    self.ml.step()
                    self.send_model()
                continue

            if recv == b'OVER':
                self.barrier[sender] = False
                self.disconnect()
                if self.nb_connected() == 0:
                    return
                else:
                    continue

            data = json.loads(self.nt.decrypt(recv))
            degree = int(data['degree'])
            del data['degree']

            self.peer_deques[sender].append((degree, util.deserialize_model(data)))

            if self.check_and_average():
                self.ml.step()
                self.send_model()
    
    def check_and_average(self):
        for n in self.peer_deques:
            if len(self.peer_deques[n]) == 0:
                return False

        total = dict()
        weight_total = 0
        for n in self.peer_deques:
            degree, data = self.peer_deques[n].popleft()
            weight = 1/(max(len(self.peer_deques), degree) + 1) # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        for key, value in self.ml.get_net().state_dict().items():
            total[key] += (1 - weight_total) * value # Metro-Hastings

        self.ml.get_net().load_state_dict(total)

        return True
