import json, zmq, random
import util, node


class GossipNode(node.Node):
    def __init__(self, rank, nodes, ML, nt, iterations, lr, logging_interval, *args):
        super().__init__(rank, nodes, ML, nt, iterations, lr, logging_interval, *args)
        
        self.age = 0

        self.peer_sockets = {}
        neighbors = nt.neighbors(self.rank)

        for rank in neighbors:
            id = str(rank).encode()
            self.barrier[id] = False
            req = self.context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.connect(nt.addr(rank))
            self.peer_sockets[id] = req

        for socket in self.peer_sockets.values():
            socket.send(b'HELLO')
        
        self.recv_loop()

    def send_model(self):
        data = util.serialize_model(self.ml.get_net())
        data['age'] = self.age

        to_send = self.nt.encrypt(json.dumps(data))
        self.log.add_sent(len(to_send))

        random_peer_socket = random.choice(list(self.peer_sockets.values()))
        random_peer_socket.send(to_send)

    def train_and_send(self):
        self.ml.step()
        self.age += 1
        self.send_model()

    def recv_loop(self):
        while True:
            sender, recvd = self.router.recv_multipart()
            self.log.add_recvd(len(recvd))

            if recvd ==b'HELLO':
                self.barrier[sender] = True
                if self.nb_connected() == len(self.peer_sockets):
                    util.Looper(self, self.logging_interval).start()
                    self.train_and_send()
                continue

            if recvd == b'OVER':
                self.barrier[sender] = False
                self.disconnect()
                if self.nb_connected() == 0:
                    return
                else:
                    continue

            recvd = json.loads(self.nt.decrypt(recvd))
            other_age = int(recvd['age'])
            del recvd['age']
            data = util.deserialize_model(recvd)

            if other_age > 0:
                other_weight = float(other_age) / (self.age + other_age)

                state_dict = self.ml.get_net().state_dict()
                for key, value in state_dict.items():
                    state_dict[key] = value * (1 - other_weight) + other_weight * data[key]

                self.ml.get_net().load_state_dict(state_dict)
                self.age = max(self.age, other_age)

            self.train_and_send()
