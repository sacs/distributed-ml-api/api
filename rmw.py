import json, zmq, random
import util, node
from collections import deque

class RMWNode(node.Node):
    def __init__(self, rank, nodes, ML, nt, iterations, lr, logging_interval, *args):
        super().__init__(rank, nodes, ML, nt, iterations, lr, logging_interval, *args)
        
        self.age = 0

        self.peer_sockets = {}
        self.peer_deques = {}

        neighbors = nt.neighbors(self.rank)
        for rank in neighbors:
            id = str(rank).encode()
            self.barrier[id] = False
            req = self.context.socket(zmq.DEALER)
            req.setsockopt(zmq.IDENTITY, self.identity)
            req.connect(nt.addr(rank))
            self.peer_sockets[id] = req
            self.peer_deques[id] = deque()

        for socket in self.peer_sockets.values():
            socket.send(b'HELLO')

        self.recv_loop()

    def send_model(self):
        data = util.serialize_model(self.ml.get_net())
        data['age'] = self.age

        to_send = self.nt.encrypt(json.dumps(data))
        self.log.add_sent(len(to_send))
        
        random_peer = random.choice(list(self.peer_sockets.keys()))

        for peer, socket in self.peer_sockets.items():
            if peer == random_peer:
                socket.send(to_send)
            else:
                socket.send(b'EMPTY')
    
    def train_and_send(self):
        self.ml.step()
        self.age += 1
        self.send_model()

    def recv_loop(self):
        while True:
            sender, recvd = self.router.recv_multipart()
            self.log.add_recvd(len(recvd))

            if recvd == b'HELLO':
                self.barrier[sender] = True
                if self.nb_connected() == len(self.peer_sockets):
                    util.Looper(self, self.logging_interval).start()
                    self.train_and_send()
                continue

            if recvd == b'OVER':
                self.barrier[sender] = False
                self.disconnect()
                if self.nb_connected() == 0:
                    return
                else:
                    continue
            
            if recvd == b'EMPTY':
                self.peer_deques[sender].append(None)
            else:
                recvd = json.loads(self.nt.decrypt(recvd))
                age = int(recvd['age'])
                del recvd['age']
                data = util.deserialize_model(recvd)

                self.peer_deques[sender].append((age, data))
            
            if self.check_and_average():
                self.train_and_send()

    def check_and_average(self):
        for d in self.peer_deques.values():
            if len(d) == 0:
                return False

        total = dict()
        for key, value in self.ml.get_net().state_dict().items():
            total[key] = self.age * value

        age_total = self.age
        max_age = self.age
        for d in self.peer_deques.values():
            item = d.popleft()
            if not item:
                continue

            age, data = item
            max_age = max(age, max_age)
            age_total += age
            for key, value in data.items():
                total[key] += value * age
        
        for key, value in total.items():
            total[key] = value / age_total

        self.ml.get_net().load_state_dict(total)
        self.age = max_age

        return True
