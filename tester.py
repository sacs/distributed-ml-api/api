import jsonlines, torch, sys, os
from torchvision import transforms, datasets
import cifar, mnist, util
from torch.multiprocessing import spawn

def main():
    FOLDER = sys.argv[1]
    NODES = int(sys.argv[2])
    ITERATIONS = int(sys.argv[3])
    spawn(fn=process, args=(FOLDER, ITERATIONS,), nprocs=NODES)

def process(rank, FOLDER, ITERATIONS):
    net = cifar.LeNet()
    transform = transforms.Compose([transforms.ToTensor(),
                        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))])
    testdata = datasets.CIFAR10('data', train=False, transform=transform)
    testset = torch.utils.data.DataLoader(testdata, batch_size=cifar.BATCH_SIZE, shuffle=False)

    input_filename = os.path.join(FOLDER, f'{rank}.jsonl')
    output_filename = os.path.join(FOLDER, f'{rank}_tested.txt')

    with jsonlines.open(input_filename) as input, open(output_filename, 'w') as output:
        iteration = 1
        for data in input:
            state_dict = net.state_dict()
            for key, value in util.deserialize_model(data).items():
                state_dict[key].copy_(value)
            net.load_state_dict(state_dict)
            accuracy = test(net, testset)
            output.write(f'Node {rank}\'s accuracy after {iteration} iteration was {accuracy*100:.3f}\n')

            iteration += 1
            if iteration > ITERATIONS:
                return

def test(net, testset):
    correct, total =  0, 0
    for data in testset:
        X, y = data
        output = net(X)
        for idx, i in enumerate(output):
            if torch.argmax(i) == y[idx]:
                correct += 1
            total += 1
    return correct/total

if __name__ == '__main__':
    main()
