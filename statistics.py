import sys, os
import numpy as np
import threading

def main():
    FOLDER = sys.argv[1]
    NODES = int(sys.argv[2])
    EPOCHS = int(sys.argv[3])

    accuracies = []
    threads = []

    for i in range(NODES):
        t = threading.Thread(target=process, args=(i, FOLDER, EPOCHS, accuracies))
        threads.append(t)
        t.start()

    for t in threads:
        t.join()


    assert len(accuracies) == NODES
    assert len(accuracies[0]) == EPOCHS

    accuracies = np.array(accuracies)

    mins = accuracies.min(axis=0)
    maxes = accuracies.max(axis=0)
    means = accuracies.mean(axis=0)
    stds = accuracies.std(axis=0)

    data = [mins, maxes, means, stds]

    output_filename = FOLDER + '_stats.txt'
    with open(output_filename, 'w') as out:
        for d in data:
            for point in d:
                out.write(f'{point:0.3f} ')
            out.write('\n')


def process(rank, FOLDER, EPOCHS, accuracies):
    my_accuracies = []
    filename = os.path.join(FOLDER, f'{rank}_tested.txt')
    epoch = 1
    last_accuracy = 0
    with open(filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line:
                blocks = line.strip().split(' ')
                last_accuracy = float(blocks[-1])
                my_accuracies.append(last_accuracy)
                epoch += 1
                if epoch == EPOCHS + 1:
                    break

    for i in range(epoch, EPOCHS + 1):
        my_accuracies.append(last_accuracy)

    accuracies.append(my_accuracies)


if __name__ == '__main__':
    main()
