import zmq, os
import util

class Node():
    def __init__(self, rank, nodes, ML, nt, iterations, lr, logging_interval, *args):
        super().__init__()

        self.rank = int(rank)
        self.iterations = int(iterations)
        self.nt = nt
        self.identity = str(self.rank).encode()
        self.ml = ML(self.rank, nodes, lr, *args)
        self.context = zmq.Context()
        self.logging_iteration = 0
        self.logging_interval = float(logging_interval)

        self.router = self.context.socket(zmq.ROUTER)
        self.router.setsockopt(zmq.IDENTITY, self.identity)
        self.router.bind(nt.addr(self.rank))

        self.sent_disconnections = False
        self.filename = os.path.join(args[0], f'{self.rank}_metrics.txt')
        self.log = util.Log()
        self.barrier = {}

    def __del__(self):
        self.context.destroy(linger=0)

    def send_model(self):
        raise NotImplementedError

    def nb_connected(self):
        counter = 0
        for val in self.barrier.values():
            if val:
                counter += 1
        return counter

    def disconnect(self):
        if not self.sent_disconnections:
            for sock in self.peer_sockets.values():
                sock.send(b'OVER')
            self.sent_disconnections = True
    
    def log_loop_iteration(self):
        self.ml.log()
        self.write_log()
        self.logging_iteration += 1

        if self.logging_iteration == self.iterations:
            self.disconnect()

    def recv_loop(self):
        raise NotImplementedError

    def write_log(self):
        with open(self.filename, 'a') as f:
            f.write(str(self.log) + '\n')

def start(NODE_TYPE, rank, nodes, ML, NT, iterations, lr, logging_interval, *args):
    NODE_TYPE(rank, nodes, ML, NT, iterations, lr, logging_interval, *args)
