import argparse, time
from torch.multiprocessing import spawn

import topologies, dpsgd, gossip, rmw
import MNIST, CIFAR

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-ps', '--procs', type=int)
    parser.add_argument('-a', '--algorithm', type=str)
    parser.add_argument('-f', '--folder_name', type=str)
    parser.add_argument('-d', '--dataset', type=str)
    parser.add_argument('-r', '--learning_rate', type=float)
    parser.add_argument('-l', '--logging_interval', type=float)
    parser.add_argument('-n', '--network', type=str)
    parser.add_argument('-i', '--iterations', type=int)
    parser.add_argument('-p', '--edge_prob', type=float, default=0.2)
    args = parser.parse_args()
    print(args)

    node = None
    if args.algorithm == 'dpsgd':
        node = dpsgd.DPSGDNode
    elif args.algorithm == 'gossip':
        node = gossip.GossipNode
    elif args.algorithm == 'rmw':
        node = rmw.RMWNode

    network = None
    if args.network == 'fc':
        network = topologies.FCNetwork(args.procs)
    elif args.network == 'ring':
        network = topologies.RINGNetwork(args.procs)
    elif args.network == 'grid':
        network = topologies.SquareGrid(args.procs)
    elif args.network == 'torus':
        network = topologies.TorusNetwork(args.procs)
    elif args.network == 'gnp':
        network = topologies.GnpRandom(args.procs, args.edge_prob)
    
    dataset = None
    if args.dataset == 'mnist':
        dataset = MNIST.MNIST
    elif args.dataset == 'cifar':
        dataset = CIFAR.CIFAR

    start = time.time()
    spawn(fn=node, nprocs=args.procs, args=[args.procs, dataset, network, args.iterations,
                        args.learning_rate, args.logging_interval, args.folder_name])
    end = time.time()

    with open(args.folder_name + '_time.txt', 'w') as f:
        f.write(str(end - start))

if __name__ == '__main__':
    main()
