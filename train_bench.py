import MNIST, CIFAR
import time
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import numpy as np

def main():
    net = CIFAR.LeNet()
    traindata = datasets.CIFAR10('data', train=True, transform=transforms.ToTensor())
    op = optim.SGD(net.parameters(), lr=0.1)

    bss = [16, 32, 64, 128, 256, 512]
    for bs in bss:
        trainset = utils.data.DataLoader(traindata, batch_size=bs)
        ffs = []
        bps = []
        gds = []
        iterations = 100

        for i, minibatch in enumerate(trainset):
            net.zero_grad()

            start = time.perf_counter()

            X, y = minibatch
            output = net(X)

            ff_finish = time.perf_counter()

            loss = F.nll_loss(output, y)
            loss.backward()

            bp_finish = time.perf_counter()

            op.step()

            gd_finish = time.perf_counter()

            ffs.append(ff_finish - start)
            bps.append(bp_finish - ff_finish)
            gds.append(gd_finish - bp_finish)

            if i == iterations - 1:
                break

        ffs, bps, gds = np.array(ffs), np.array(bps), np.array(gds)

        print(f'{ffs.min()*1000};{ffs.max()*1000};{ffs.mean() * 1000};{ffs.std() * 1000}')
        print(f'{bps.min()*1000};{bps.max()*1000};{bps.mean() * 1000};{bps.std() * 1000}')
        print(f'{gds.min()*1000};{gds.max()*1000};{gds.mean() * 1000};{gds.std() * 1000}')


if __name__ == '__main__':
    main()
