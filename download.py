from torchvision import datasets, transforms
import sys

def main():
    name = sys.argv[1]
    if name == 'mnist':
        datasets.MNIST('data', train=True, download=True, transform=transforms.ToTensor())
        datasets.MNIST('data', train=False, download=True, transform=transforms.ToTensor())
    elif name == 'cifar10':
        datasets.CIFAR10('data', train=True, download=True, transform=transforms.ToTensor())
        datasets.CIFAR10('data', train=False, download=True, transform=transforms.ToTensor())

if __name__ == '__main__':
    main()
