import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import jsonlines, os, random
import numpy as np

import util, api


BATCH_SIZE = 128

class LeNet(nn.Module):
    """
    Inspired by original LeNet network for MNIST: https://ieeexplore.ieee.org/abstract/document/726791
    Layer parameters taken from: https://github.com/kevinhsieh/non_iid_dml/blob/master/apps/caffe/examples/cifar10/1parts/gnlenet_train_val.prototxt.template
    (with Group Normalisation).
    Results for previous model described in http://proceedings.mlr.press/v119/hsieh20a.html
    """

    def __init__(self, output=10):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=5, stride=1, padding=2),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.GroupNorm(2, 32),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, kernel_size=5, stride=1, padding=2),
            nn.GroupNorm(2, 32),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.GroupNorm(2, 64),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.ReLU(inplace=True),
        )
        self.classifier = nn.Linear(576, output)

    def forward(self, x):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return F.log_softmax(x, dim=1)


class CIFAR(api.LocalMain):
    def __init__(self, rank, world_size, lr, *args):
        super().__init__(rank, world_size)
        torch.manual_seed(0)
        np.random.seed(0)

        self.net = LeNet()
        transform = transforms.Compose([transforms.ToTensor(),
                        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))])
        traindata = datasets.CIFAR10('data', train=True, transform=transform)

        rand_perm = np.arange(len(traindata))
        np.random.shuffle(rand_perm)

        nb_per_node = len(traindata) // world_size
        offset = rank * nb_per_node

        train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])

        trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True)
        self.trainset = list(trainset)
        self.lr = lr
        self.optim = optim.SGD(self.net.parameters(), lr=self.lr)

        # self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False)

        self.index = 0
        self.epoch = 0

        self.filename = os.path.join(args[0], f'{self.rank}.jsonl')

        # TODO remove
        # with jsonlines.open(self.filename, 'r') as input:
        #     for data in input:
        #         state_dict = self.net.state_dict()
        #         for key, value in util.deserialize_model(data).items():
        #             state_dict[key].copy_(value)
        #         self.net.load_state_dict(state_dict)
        #         self.epoch += 1

    def get_net(self):
        return self.net

    def step(self):
        X, y = self.trainset[self.index]
        self.net.zero_grad()
        output = self.net(X)
        loss = F.nll_loss(output, y)
        loss.backward()
        self.optim.step()
        self.index += 1
        if self.index == len(self.trainset):
            self.epoch += 1
            self.index = 0
            random.shuffle(self.trainset)
            # self.test()
            # Learning rate decays by 10% every 10 epochs
            if self.epoch % 10 == 0:
                self.lr *= 0.9
                self.optim = optim.SGD(self.net.parameters(), lr=self.lr)

    def log(self):
        if not os.path.exists(self.filename):
            with open(self.filename, 'w') as file:
                pass

        with jsonlines.open(self.filename, mode='a') as file:
            file.write(util.serialize_model(self.net))

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = self.net(X)
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank}: accuracy after {self.epoch} epochs: {correct / total:.4f}')
