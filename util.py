import torch, numpy, json, time
from threading import Thread

"""
    Deserializes the given dictionary into a state dictionary
    which can be reconstructed into a model.
"""
def deserialize_model(j: dict):
    state_dict = dict()
    for key, value in j.items():
        state_dict[key] = torch.from_numpy(numpy.array(json.loads(value)))

    return state_dict

"""
    Serializes the given model as a dictionary
    It passes through a conversion to a numpy array to be able to
    deserialize from a json format.
"""
def serialize_model(model: torch.nn.Module):
    j = {}
    for key, value in model.state_dict().items():
        j[key] = json.dumps(value.numpy().tolist())
    
    return j

class Looper(Thread):
    def __init__(self, node, interval):
        Thread.__init__(self)
        self.node = node
        self.interval = interval

    def run(self):
        while not self.node.sent_disconnections:
            start = time.time()
            self.node.log_loop_iteration()
            time.sleep(max(self.interval - (time.time() - start), 0))

class Log():
    def __init__(self):
        super().__init__()
        self.start = time.time()
        self.sent = 0
        self.received = 0

    def add_sent(self, nb):
        self.sent += nb

    def add_recvd(self, nb):
        self.received += nb

    def __str__(self):
        return  f'Time:{time.time() - self.start}|Sent:{self.sent}|Received:{self.received}'
