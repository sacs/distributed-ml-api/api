import MNIST, CIFAR
import time
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import numpy as np

def main():
    nets = []
    ops = []
    MAX_DEGREE = 50
    for _ in range(MAX_DEGREE):
        net = CIFAR.LeNet()
        nets.append(net)
        ops.append(optim.SGD(net.parameters(), lr=0.1))

    traindata = datasets.CIFAR10('data', train=True, transform=transforms.ToTensor())
    trainset = utils.data.DataLoader(traindata, batch_size=CIFAR.BATCH_SIZE)

    for i, batch in enumerate(trainset):
        net = nets[i]
        op = ops[i]
        net.zero_grad()

        X, y = batch
        output = net(X)
        loss = F.nll_loss(output, y)
        loss.backward()
        op.step()

        if i == MAX_DEGREE - 1:
            break

    result = CIFAR.LeNet()
    rsd = result.state_dict()
    for key, value in rsd.items():
        rsd[key] = value * 0
    result.load_state_dict(rsd)

    iterations = 200
    for degree in range(2, MAX_DEGREE + 1):
        durations = []
        for _ in range(iterations):
            rsd = result.state_dict()
            for key, value in rsd.items():
                rsd[key] = value * 0

            start = time.perf_counter()

            for i in range(degree):
                for key, value in nets[i].state_dict().items():
                    rsd[key] += value / degree

                result.load_state_dict(rsd)

            durations.append(time.perf_counter() - start)

        npa = np.array(durations)
        print(f'{npa.min()*1000};{npa.max()*1000};{npa.mean() * 1000};{npa.std() * 1000}')

if __name__ == '__main__':
    main()
