import api
import math
from lz4.frame import compress, decompress
import networkx as nx

class CompressionLayer(api.Network):
    def encrypt(self, s: str):
        return compress(s.encode('utf-8'))
    
    def decrypt(self, b: bytes):
        return decompress(b).decode('utf-8')


class GnpRandom(CompressionLayer):
    def __init__(self, n, p) -> None:
        super().__init__()
        self.graph = nx.gnp_random_graph(n, p)

    def neighbors(self, rank):
        return self.graph[rank]


class FCNetwork(CompressionLayer):
    def __init__(self, n) -> None:
        super().__init__()
        self.n = n

    def neighbors(self, rank):
        neighbors = set()
        for r in range(self.n):
            if r != rank:
                neighbors.add(r)
        return neighbors


class RINGNetwork(CompressionLayer):
    def __init__(self, n) -> None:
        super().__init__()
        self.n = n

    def neighbors(self, rank):
        neighbors = set()
        neighbors.add((rank + 1) % self.n)
        neighbors.add((rank - 1) % self.n)
        return neighbors


class SquareGrid(CompressionLayer):
    def __init__(self, n) -> None:
        super().__init__()
        self.n = n

    def neighbors(self, rank):
        side = round(math.sqrt(self.n))
        def find_rank(row, col):
            return row * side + col

        r = rank // side
        c = rank % side

        neighbors = set()

        if r - 1 >= 0:
            neighbors.add(find_rank(r - 1, c))
        if r + 1 < side:
            neighbors.add(find_rank(r + 1, c))

        if c - 1 >= 0:
            neighbors.add(find_rank(r, c - 1))
        if c + 1 < side:
            neighbors.add(find_rank(r, c + 1))

        return neighbors


class TorusNetwork(CompressionLayer):
    def __init__(self, n) -> None:
        super().__init__()
        self.n = n

    def neighbors(self, rank):
        side = round(math.sqrt(self.n))
        def find_rank(row, col):
            return row * side + col
        
        r = rank // side
        c = rank % side

        neighbors = set()
        neighbors.add(find_rank((r + 1) % side, c))
        neighbors.add(find_rank((r - 1) % side, c))
        neighbors.add(find_rank(r, (c + 1) % side))
        neighbors.add(find_rank(r, (c - 1) % side))

        return neighbors
