import torch
from torch import nn, utils, optim
import torch.nn.functional as F
from torchvision import transforms, datasets
import jsonlines, os
import numpy as np
import util, api
import random

SIZE = 28 * 28
BATCH_SIZE = 128

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(SIZE, 10)

    def forward(self, x):
        x = self.fc1(x)
        return F.log_softmax(x, dim=1)

# Example DML class for the MNIST dataset and a homogeneous data distribution
class MNIST(api.LocalMain):
    def __init__(self, rank, world_size, lr, *args):
        super().__init__(rank, world_size)
        torch.manual_seed(0)
        np.random.seed(0)

        self.net = Net()
        traindata = datasets.MNIST('data', train=True, transform=transforms.ToTensor())
        # testdata = datasets.MNIST('data', train=False, transform=transforms.ToTensor())

        rand_perm = np.arange(len(traindata))
        np.random.shuffle(rand_perm)

        nb_per_node = len(traindata) // world_size
        offset = rank * nb_per_node

        train_subset = utils.data.Subset(traindata, rand_perm[offset:offset+nb_per_node])

        trainset = utils.data.DataLoader(train_subset, batch_size=BATCH_SIZE, shuffle=True)
        self.trainset = list(trainset)

        # self.testset = utils.data.DataLoader(testdata, batch_size=BATCH_SIZE, shuffle=False)
        self.optim = optim.SGD(self.net.parameters(), lr=lr)

        self.index = 0
        self.epoch = 0
        self.filename = os.path.join(args[0], f'{self.rank}.jsonl')

    def get_epoch(self):
        return self.epoch

    def get_net(self):
        return self.net

    def step(self):
        X, y = self.trainset[self.index]
        self.net.zero_grad()
        output = self.net(X.view(-1, SIZE))
        loss = F.nll_loss(output, y)
        loss.backward()
        self.optim.step()
        self.index += 1
        if self.index == len(self.trainset):
            self.epoch += 1
            self.index = 0
            random.shuffle(self.trainset)
            # self.test()

    def log(self):
        if not os.path.exists(self.filename):
            with open(self.filename, 'w') as file:
                pass

        with jsonlines.open(self.filename, mode='a') as file:
            file.write(util.serialize_model(self.net))

    def test(self):
        correct, total =  0, 0
        with torch.no_grad():
            for data in self.testset:
                X, y = data
                output = self.net(X.view(-1, SIZE))
                for idx, i in enumerate(output):
                    if torch.argmax(i) == y[idx]:
                        correct += 1
                    total += 1

        print(f'Node {self.rank}: accuracy after {self.epoch} epochs: {correct / total:.3f}')
