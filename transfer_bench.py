import MNIST, CIFAR, util
import zmq, time, sys, json
import numpy as np

def count_parameters(model):
    total_params = 0
    for name, parameter in model.named_parameters():
        if not parameter.requires_grad: continue
        param = parameter.numel()
        total_params+=param
    print(f"Total Trainable Params: {total_params}")
    return total_params

def main():
    context = zmq.Context()
    router = context.socket(zmq.ROUTER)
    rank = int(sys.argv[1])
    req = context.socket(zmq.DEALER)
    req.probe_router = 1
    router.bind(f'tcp://127.0.0.1:2000{rank}')
    req.connect(f'tcp://127.0.0.1:2000{(rank + 1) % 2}')
    # if rank == 0:
    #     router.bind(f'tcp://10.90.41.128:20000')
    #     req.connect(f'tcp://10.90.41.129:20000')
    # else:
    #     router.bind(f'tcp://10.90.41.129:20000')
    #     req.connect(f'tcp://10.90.41.128:20000')

    if rank == 0:
        req.send(b'hello')
        rcvd = None
        while not rcvd:
            _, rcvd = router.recv_multipart()
        print(f'received {rcvd}')
        rcvd = None
        net = MNIST.Net()
        iterations = 500
        durations = []

        for _ in range(iterations):
            start = time.perf_counter()

            to_send = json.dumps(util.serialize_model(net)).encode()
            req.send(to_send)
            _, data = router.recv_multipart()
            data = util.deserialize_model(json.loads(data.decode()))

            durations.append(time.perf_counter() - start)

        npa = np.array(durations) / 2
        print(f'{npa.min()*1000},{npa.max()*1000},{npa.mean() * 1000},{npa.std() * 1000}')
    else:
        net = MNIST.Net()
        while True:
            data = None
            while not data:
                _, data = router.recv_multipart()
            if data != b'hello':
                data = util.deserialize_model(json.loads(data.decode()))
                data = json.dumps(util.serialize_model(net)).encode()
            req.send(data)

if __name__ == '__main__':
    main()
